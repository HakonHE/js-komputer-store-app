# Komputer-Store-APP

A simulated computer store with features like:
 - Checking bank balance
 - Taking up a loan
 - Working for money
 - Banking that money in your bank account
 - Checking different laptops and specs
 - Buying the selected laptop if you have the money for it

## Author

Håkon Holm Erstad



