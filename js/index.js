// MONEY/BALANCE ELEMENTS
const balanceEl = document.getElementById("balance")
const outstandingEl = document.getElementById("outstanding")
const payEl = document.getElementById("pay")

// BUTTON ELEMENTS SECOND ROW
const getALoanBtnEl = document.getElementById("getALoanButton")
const bankCashBtnEl = document.getElementById("bankCashButton")
const workBtnEl = document.getElementById("workButton")
const repayBtnEl = document.getElementById("repayButton")

// LAPTOP SELECTION ELEMENTS
const laptopsEl = document.getElementById("laptops")
const featuresEl = document.getElementById("features")

// LAPTOP BUY SCREEN ELEMENTS
const laptopImageEl = document.getElementById("laptopImage")
const laptopNameEl = document.getElementById("laptopName")
const laptopDescriptionEl = document.getElementById("laptopDescription")
const laptopPriceEl = document.getElementById("laptopPrice")
const buyBtnEl = document.getElementById("buyButton")

// FOr showing bank currency
// new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number));

// GLOBAL VARIABLES
let balance = 0
let loan = 0
let salary = 0;
let laptops = []
let selectedLaptop

// FETCH FROM API

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))

// ADD TO SELECT MENU FUNCTIONS

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x))
    for (feature of laptops[0].specs) {
        featuresEl.insertAdjacentText("beforeend", `${feature}. `)
    }
    updateInfo(laptops[0])
    selectedLaptop = laptops[0]
}

const addLaptopToMenu = (laptop) => {
    const laptopEl = document.createElement("option")
    laptopEl.appendChild(document.createTextNode(laptop.title))
    laptopsEl.appendChild(laptopEl)
}


// UPDATE/RENDER FUNCTIONS

function updateBankBalance(amount) {
    const currencyBalance = (new Intl.NumberFormat('no-No', { style: 'currency', currency: 'NOK', maximumFractionDigits: 0}).format(amount))
    balanceEl.innerText = currencyBalance
}

// UPDATING THE OUTSTANDING LOAN AFTER TAKING UP A LOAN
function updateOutstanding(amount) {
    if (loan === 0) {
        outstandingEl.innerText = ""
        repayBtnEl.style.display = "none"
    }
    else {
        const currencyOutstanding = (new Intl.NumberFormat('no-No', { style: 'currency', currency: 'NOK', maximumFractionDigits: 0}).format(amount))
        outstandingEl.innerText = `Outstanding Loan: ${currencyOutstanding}`
        repayBtnEl.style.display = ''
    }
}

// UPDATING THE PAY AFTER DOING WORK
function updatePay(amount) {
    const currencyPay = (new Intl.NumberFormat('no-No', { style: 'currency', currency: 'NOK', maximumFractionDigits: 0}).format(amount))
    payEl.innerText = currencyPay
}

// UPDATING INFO FOR THE SELECTED LAPTOP
function updateInfo(laptop) {
    const currencyLaptopPrice = (new Intl.NumberFormat('no-No', { style: 'currency', currency: 'NOK', maximumFractionDigits: 0}).format(laptop.price))
    
    laptopImageEl.src = `https://noroff-komputer-store-api.herokuapp.com/${laptop.image}`
    laptopNameEl.innerText = laptop.title
    laptopDescriptionEl.innerText = laptop.description
    laptopPriceEl.innerText = currencyLaptopPrice
}

// HANDLER FOR CHANGE IN SELECTED LAPTOP
const handleLaptopMenuChange = e => {
    featuresEl.innerText = ""
    laptopNameEl.innerText = ""
    laptopDescriptionEl.innerText = ""
    laptopPriceEl.innerText = ""

    selectedLaptop = laptops[e.target.selectedIndex]
    for (feature of selectedLaptop.specs){
        featuresEl.insertAdjacentText("beforeend", `${feature}. `)
    }
    updateInfo(selectedLaptop)
}

// INITIALIZING STARTING INFO
updateBankBalance(balance)
updatePay(salary)


// EVENTLISTENERS FOR BUTTONS

getALoanBtnEl.addEventListener("click", () => {
    if(loan > 0) {
        alert("Can't have more than one loan, pay it back to be able to get another one.")
    }
    else {
        const loanAmount = Number(window.prompt(`How much do you need to loan?(Max amount: ${balance * 2})`, ""))
        if(loanAmount <= balance * 2) {
            loan += loanAmount
            balance += loanAmount
            updateOutstanding(loan)
            updateBankBalance(balance)
        }
        else {
            alert("Invalid amount entered, remember to input a number that is less or equal to double your balance.")
        }
    }  
})

// BUTTON FOR BANKING THE CASH EARNED FROM WORK
bankCashBtnEl.addEventListener("click", () => {
    if(loan === 0) {
        balance += salary
        salary = 0
        updateBankBalance(balance)
        updatePay(salary)
    }
    else {
        const deduction = salary / 10
        loan -= deduction
        balance += salary - deduction
        salary = 0
        if (loan < 0) {
            balance -= loan
            loan = 0
        }
        updateBankBalance(balance)
        updateOutstanding(loan)
        updatePay(salary)
    }
})

// A BUTTON FOR WORKING AND EARNING PAY
workBtnEl.addEventListener("click", () => {
    salary += 100
    updatePay(salary)
})

// BUTTON FOR REPAYING STRAIGHT TO LOAN
repayBtnEl.addEventListener("click", () => {
	loan -= salary
	salary = 0
	if (loan < 0 || loan == 0) {
		balance -= loan
		loan = 0
	}
	updateBankBalance(balance)
	updateOutstanding(loan)
	updatePay(salary)
})

// THE BUY BUTTON FOR BUYING A LAPTOP
buyBtnEl.addEventListener("click", () => {
    const laptopPrice = selectedLaptop.price
    if(balance < laptopPrice) {
        alert("You don't have enough money to buy this laptop.")
    }
    else {
        balance -= laptopPrice
        updateBankBalance(balance)
        alert(`You are now the owner of the ${selectedLaptop.title} laptop`)
    }
})

// EVENTLISTENER FOR SELECT EVENT
laptopsEl.addEventListener("change", handleLaptopMenuChange)